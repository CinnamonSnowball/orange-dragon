
-- region : Modules & Definitions
-- ############################################################################################## --

local Action = require("necro.game.system.Action")
local AI = require("necro.game.enemy.ai.AI")
local Attack = require("necro.game.character.Attack")
local Collision = require("necro.game.tile.Collision")
local CommonSpell = require("necro.game.data.spell.CommonSpell")
local CustomEntities = require("necro.game.data.CustomEntities")
local Event = require("necro.event.Event")
local Entities = require("system.game.Entities")
local Object = require("necro.game.object.Object")

-- ############################################################################################## --
-- endregion
-- region
-- ############################################################################################## --

CustomEntities.extend({name = "Dragon5",
    template = CustomEntities.template.enemy("dragon", 2),
    components = {
        innateSpellcasts = {
            mapping = {
                [Action.Special.SPELL_1] = {
                    direction = 5,
                    spell = "OrangeDragon_SpellcastFireballDragonOrange"
                },
            }
        },
        autoCastActivationLinear = {
            directions = {
                [Action.Direction.RIGHT] = true,
                [Action.Direction.UP_RIGHT] = true,
                [Action.Direction.UP] = true,
                [Action.Direction.UP_LEFT] = true,
                [Action.Direction.LEFT] = true,
                [Action.Direction.DOWN_LEFT] = true,
                [Action.Direction.DOWN] = true,
                [Action.Direction.DOWN_RIGHT] = true,
            },
            maxDistance = 9,
        },
        autoCastDirectionalInput = {
            -- Auto-cast input mapping
            inputs = {
                [Action.Direction.RIGHT] = Action.Special.SPELL_1,
                [Action.Direction.UP_RIGHT] = Action.Special.SPELL_1,
                [Action.Direction.UP] = Action.Special.SPELL_1,
                [Action.Direction.UP_LEFT] = Action.Special.SPELL_1,
                [Action.Direction.LEFT] = Action.Special.SPELL_1,
                [Action.Direction.DOWN_LEFT] = Action.Special.SPELL_1,
                [Action.Direction.DOWN] = Action.Special.SPELL_1,
                [Action.Direction.DOWN_RIGHT] = Action.Special.SPELL_1,
            },
        },
        beatDelay = {interval = 1},
        knockbackable = {minimumDistance = 1},
        ai = {
            id = AI.Type.SEEK_LAZY,
            directions = AI.allDirections
        },
        sprite = {
            texture = "mods/OrangeDragon/entities/dragon_orange.png",
        },
    }
})

Event.levelLoad.add("replaceRedDragons",
    {order = "entities", sequence = 1},
    function ()
        local dragon2 = Entities.getEntitiesByType("Dragon2")
        while dragon2:next() do
            Object.convert(dragon2, "OrangeDragon_Dragon5")
        end
    end
)

CommonSpell.registerSpell("SpellcastFireballDragonOrange", {
    spellcastLinear = {
        collisionMask = Collision.Type.WALL,
    },
    spellcastMulticast = {
        castCount = 8,
        rotation = Action.Rotation.CW_45,
    },
    spellcastInflictDamage = {
        damage = 5,
        attackFlags = Attack.mask(
            Attack.Flag.DEFAULT,
            Attack.Flag.PROVOKE,
            Attack.Flag.IGNORE_TEAM
        ),
        useCastDirection = true,
    },
    spellcastConvertTiles = {
        types = {
            Water = "Floor",
            Ice = "Water",
        },
    },
    spellcastDirectionalOffset = {distance = 1},
    spellcastSwipe = {
        textures = {
            "ext/spells/fire0.png",
            "ext/spells/fire1.png",
            "ext/spells/fire2.png",
            "ext/spells/fire3.png",
            "ext/spells/fire4.png",
        },
        width = 24,
        height = 24,
        frameCount = 7,
        duration = 400,
        diagonalScale = math.sqrt(2),
    },
    spellcastMultiTileSwipe = {},
    soundSpellcast = {sound = "dragonFire"},
})

-- ############################################################################################## --
-- endregion

